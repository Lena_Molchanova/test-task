<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit606fc74cd25d57c4326f82cfac77100d
{
    public static $classMap = array (
        'ComposerAutoloaderInit606fc74cd25d57c4326f82cfac77100d' => __DIR__ . '/..' . '/composer/autoload_real.php',
        'Composer\\Autoload\\ClassLoader' => __DIR__ . '/..' . '/composer/ClassLoader.php',
        'Composer\\Autoload\\ComposerStaticInit606fc74cd25d57c4326f82cfac77100d' => __DIR__ . '/..' . '/composer/autoload_static.php',
        'Controller' => __DIR__ . '/..' . '/framework/Controller.php',
        'Database' => __DIR__ . '/..' . '/framework/Database.php',
        'Model' => __DIR__ . '/..' . '/framework/Model.php',
        'PrepareSqlHelper' => __DIR__ . '/../..' . '/application/helpers/PrepareSqlHelper.php',
        'Route' => __DIR__ . '/..' . '/framework/Route.php',
        'Student' => __DIR__ . '/../..' . '/application/models/Student.php',
        'StudentController' => __DIR__ . '/../..' . '/application/controllers/StudentController.php',
        'Validator' => __DIR__ . '/../..' . '/application/helpers/Validator.php',
        'View' => __DIR__ . '/..' . '/framework/View.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInit606fc74cd25d57c4326f82cfac77100d::$classMap;

        }, null, ClassLoader::class);
    }
}

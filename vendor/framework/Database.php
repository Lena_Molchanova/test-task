<?php

class Database 
{
	private $connect;
	private static $_instance;

	private function __construct() 
	{
		$dsn = 'mysql:host=' . HOST . ';dbname=' . DB . ';charset=utf8';

		$opt = [
	        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
	        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
	        PDO::ATTR_EMULATE_PREPARES   => false,
	    ];
    
		$this->connect = new PDO($dsn, USER, PASSWORD, $opt);
	}

	protected function __clone() {}

	static public function getInstance() 
	{
		if(is_null(self::$_instance)) {

			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function getConnect() 
	{
		return $this->connect;
	}
}

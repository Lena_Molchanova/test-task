<?php

class Route 
{
	static function start() 
	{
		$controller = new StudentController;

		$url   = $_SERVER['REQUEST_URI'];
		$route = explode('/', $url);

		$action = !empty($route[1]) ? $route[1] : 'index';
		$action = strpos($route[1], '?') ? substr($route[1], 0, strpos($route[1], '?')) : $action;

		$actionName = $action . 'Action';

		if (!method_exists($controller, $actionName)) {
			
			static::errorPage404();
		}

		call_user_func(array($controller, $actionName));
	}

	static function errorPage404(){
		die(header("HTTP/1.1 404 Not Found"));
	}
}

-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Час створення: Жов 07 2017 р., 15:17
-- Версія сервера: 5.7.19-0ubuntu0.17.04.1
-- Версія PHP: 7.0.22-0ubuntu0.17.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `testtask`
--

-- --------------------------------------------------------

--
-- Структура таблиці `students`
--

CREATE TABLE `students` (
  `id` int(15) NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `gender` varchar(6) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `group_number` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `point_sum` int(3) NOT NULL,
  `year_birth` int(4) NOT NULL,
  `residence_status` varchar(5) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп даних таблиці `students`
--

INSERT INTO `students` (`id`, `first_name`, `last_name`, `gender`, `group_number`, `email`, `point_sum`, `year_birth`, `residence_status`) VALUES
(6, 'Ivan1', 'Ivanov', 'male', '12', 'andlee11@gmail.com', 123, 1998, 'local'),
(7, 'Red1', 'Vores', 'male', '1334', 'red@example.com', 144, 1990, 'not'),
(9, 'Ivan2', 'Ivanov', 'male', '12', 'andlee111@gmail.com', 123, 1998, 'local'),
(10, 'Ivan3', 'Ivanov', 'male', '12', 'andlee1111@gmail.com', 123, 1998, 'local'),
(11, 'Red2', 'Vores', 'male', '1334', 'red1@example.com', 144, 1990, 'not'),
(12, 'Red3', 'Vores', 'male', '1334', 'red11@example.com', 144, 1990, 'not'),
(13, 'Ivan4', 'Ivanov', 'male', '12', 'andlee11111@gmail.com', 123, 1998, 'local');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `students`
--
ALTER TABLE `students`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

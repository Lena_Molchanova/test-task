<?php

class Student extends Model
{
	protected $tableName = 'students';

	protected $fieldNames = [
		'first_name'       => '',
		'last_name'        => '',
		'gender'           => '',
		'group_number'     => '',
		'email'            => '',
		'point_sum'        => '',
		'year_birth'       => '',
		'residence_status' => '',
	];

	public static function basicValidateRules($data)
   	{   
        $rules = [            
            'first_name'       => ['isntEmpty'],
            'last_name'        => ['isntEmpty'],
            'gender'           => ['isntEmpty'],
            'group_number'     => ['isntEmpty'],
            'email'            => ['isitEmail',
                                   'isntExistInTable'],
            'point_sum'        => ['isntEmpty',
            				       'isitNumber',
            					   'isSumCorrect'        => [100, 200]],
            'year_birth'       => ['isntEmpty',
            				       'isitNumber',
            				       'isLenghtCorrect'     => [4, 4]],
            'residence_status' => ['isntEmpty']
        ];
        return $rules;
    }

    public static function editValidateRules($data)
   	{   
        $rules = [            
            'first_name'       => ['isntEmpty'],
            'last_name'        => ['isntEmpty'],
            'gender'           => ['isntEmpty'],
            'group_number'     => ['isntEmpty'],
            'email'            => ['isitEmail',
                              	   'isntExistInTableExceptMe'],
            'point_sum'        => ['isntEmpty',
            				       'isitNumber',
            					   'isSumCorrect'        => [100, 200]],
            'year_birth'       => ['isntEmpty',
            				       'isitNumber',
            				       'isLenghtCorrect'     => [4, 4]],
            'residence_status' => ['isntEmpty']
        ];
        return $rules;
    }

	public function getAllStudents($orderBy = 'point_sum', $sort = 'ASC') 
    {
		$sql = "SELECT * from {$this->tableName} ORDER BY {$orderBy} {$sort}";

		$query = $this->connect->prepare($sql);
		$query->execute();

		$students = $query->fetchAll(PDO::FETCH_ASSOC);

		return $students;
	}

	public function getFieldBy($getField, $value) 
	{        
        $sql = "SELECT {$getField} FROM {$this->tableName} WHERE {$getField} = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$value]);
        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public function save($data)
    {
    	$prepareSqlHelper = new PrepareSqlHelper($data, $this->tableName, $this->fieldNames);
    	$sql = $prepareSqlHelper->savePrepare();

    	$query = $this->connect->prepare($sql);
        $query->execute();

        $lastId = $this->connect->lastInsertId();
        
        return $lastId;
    }

    public function getStudent($id)
    {
    	$sql = "SELECT * FROM {$this->tableName} WHERE id = ? LIMIT 1";

    	$query = $this->connect->prepare($sql);
    	$query->execute([$id]);

    	$result = $query->fetch(PDO::FETCH_ASSOC);

    	return $result;
    }

    public function update($id, $data)
    {
    	$prepareSqlHelper = new PrepareSqlHelper($data, $this->tableName, $this->fieldNames);
    	$sql = $prepareSqlHelper->updatePrepare();
        
        $query = $this->connect->prepare($sql);
        $query->execute([$id]);     
    }

    public function getFieldExceptMeBy($getField, $value)
    {        
        $sql = "SELECT {$getField} FROM {$this->tableName} WHERE {$getField} = ? AND id NOT LIKE ? ";

        $query = $this->connect->prepare($sql);
        $query->execute([$value,
                         $_COOKIE['registerId']]);
        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

}

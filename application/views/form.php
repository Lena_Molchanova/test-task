<div class="container register-from col-md-6">

  <div class="row main ">
    <div class="panel-heading">
        <h1 class="title"><?= isset($_COOKIE['register']) ? 'Edit' : 'Register'; ?></h1>
        <hr />
      </div>
    </div>

    <?php if(isset($errors)): ?>
      <div class="errors">
        <?php foreach ($errors as $error) : ?>
          <div class="alert alert-danger">
            <?=$error;?>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>

    <div class="main-login main-center">
      <form enctype="form-data" class="" method="post" action="<?= isset($_COOKIE['register']) ? '/edit' : '/register'; ?>">

        <div class="form-row">
        <div class="form-group col-md-6">
          <label for="name" class="cols-sm-2 control-label">Your First Name</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <input maxlength="255" type="text" class="form-control" name="student[first_name]" id="name"  placeholder="Enter your First Name" value="<?= isset($studentData['first_name']) ? $studentData['first_name'] : ''; ?>"/>
            </div>
          </div>
        </div>

        <div class="form-group col-md-6">
          <label for="name" class="cols-sm-2 control-label">Your Last Name</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <input maxlength="255" type="text" class="form-control" name="student[last_name]" id="name"  placeholder="Enter your Last Name" value="<?= isset($studentData['last_name']) ? $studentData['last_name'] : '';?>"/>
            </div>
          </div>
        </div>
        </div>

        <div class="form-group">
        <legend class="col-form-legend">Gender</legend>
        <div class="col-sm-10">
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="student[gender]" id="gridRadios1" value="female" 
              <?php if(isset($studentData['gender']) && $studentData['gender'] == 'female') echo "checked"; ?> > I am female
            </label>
          </div>
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="student[gender]" id="gridRadios2" value="male"
              <?php if(isset($studentData['gender']) && $studentData['gender'] == 'male') echo 'checked'; ?> >
              I am male
            </label>
          </div>
        </div>
        </div>

        <div class="form-group">
          <label for="group_number" class="cols-sm-2 control-label">Group Number</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <input maxlength="6" type="text" class="form-control" name="student[group_number]" id="group_number"  placeholder="Enter Group Number" value="<?= isset($studentData['group_number']) ? $studentData['group_number'] : '';?>"/>
            </div>
          </div>
        </div>


        <div class="form-group">
          <label for="email" class="cols-sm-2 control-label">Your Email</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <input maxlength="255" type="text" class="form-control" name="student[email]" id="email"  placeholder="Enter your Email" value="<?= isset($studentData['email']) ? $studentData['email'] : '';?>"/>
            </div>
          </div>
        </div>


        <div class="form-group">
          <label for="point_sum" class="cols-sm-2 control-label">Points</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <input maxlength="3" type="text" class="form-control" name="student[point_sum]" id="point_sum"  placeholder="Enter your Point Sum" value="<?= isset($studentData['point_sum']) ? $studentData['point_sum'] : '';?>"/>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="year_birth" class="cols-sm-2 control-label">Birth Year</label>
          <div class="cols-sm-10">
            <div class="input-group">
              <input maxlength="4" type="text" class="form-control" name="student[year_birth]" id="year_birth"  placeholder="Enter your Birth Year" value="<?= isset($studentData['year_birth']) ? $studentData['year_birth'] : '';?>"/>
            </div>
          </div>
        </div>

        <div class="form-group">
        <legend class="col-form-legend">Residence Status</legend>
        <div class="col-sm-10">
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="student[residence_status]" id="gridRadios1" value="local"
              <?php if(isset($studentData['residence_status']) && $studentData['residence_status'] == 'local') echo "checked"; ?>> I am local
            </label>
          </div>
          <div class="form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="radio" name="student[residence_status]" id="gridRadios2" value="not" 
              <?php if(isset($studentData['residence_status']) && $studentData['residence_status'] == 'not') echo "checked"; ?>>
              I am not local
            </label>
          </div>
        </div>
        </div>


        <div class="form-group">
          <input type="submit" class="btn btn-primary btn-lg btn-block login-button"  value="<?= isset($_COOKIE['register']) ? 'Edit' : 'Register'; ?>" / >
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="assets/js/bootstrap.js"></script>

<h1>Students List</h1>

<table class="table">
  <thead class="thead-inverse">
    <tr>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Goup Number</th>
      <th>Points</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($students as $student) : ?>
      <tr>
        <td><?= $student['first_name']; ?></td>
        <td><?= $student['last_name']; ?></td>
        <td><?= $student['group_number']; ?></td>
        <td><?= $student['point_sum']; ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<?php if(!isset($_GET['page']) || $_GET['page'] == '1' ) : ?>

  <nav aria-label="...">
    <ul class="pagination">

      <li class="page-item active">
        <a class="page-link" href="/">1</a>
      </li>

      <?php for ($i=2 ; $i <= $countPage; $i++) : ?>
        <li class="page-item"><a class="page-link" href="/index?page=<?= $i; ?>">
          <?= $i; ?></a>
        </li>
      <?php endfor; ?>

      <?php if($countPage > 1) : ?>
        <li class="page-item">
          <a class="page-link" href="/index?page=<?= ($_GET['page'] + 1); ?>">Next</a>
        </li>
      <?php endif; ?>
    </ul>
  </nav>

<?php else : ?>

  <nav aria-label="...">
    <ul class="pagination">
      <li class="page-item">
        <a class="page-link " href="/index?page=<?= ($_GET['page'] - 1); ?>">Previous</a>
      </li>  
      <?php for ($i=1 ; $i <= $countPage; $i++) : ?>
        <li class="page-item <?= ($_GET['page'] == $i) ? 'active' : ''; ?>">
          <a class="page-link" href="/index?page=<?= $i; ?>"><?= $i; ?></a>
        </li>
      <?php endfor; ?>
      <?php if(($_GET['page']) < $countPage) : ?>
        <li class="page-item">
          <a class="page-link" href="/index?page=<?= ($_GET['page'] + 1); ?>">Next</a>
        </li>
      <?php endif; ?>
    </ul>
  </nav>

<?php endif; ?>
<?php

class PrepareSqlHelper
{
	protected $data;
	protected $tableName;
	protected $fieldNames;

	public function __construct($data, $tableName, $fieldNames)
	{
		$this->data       = $data;
		$this->tableName  = $tableName;
		$this->fieldNames = $fieldNames;
	}

    public function savePrepare()
    {
        $prepareData = $this->prepareData();

        $sql = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            $this->tableName,
            implode(', ', array_keys($prepareData)),
            "'" . implode("', '", $prepareData) . "'"
        );

        return $sql; 
    }

	public function updatePrepare()
    {
        $prepareData = $this->prepareData();

        $pieceSql = implode(' =?, ', array_keys($prepareData)) . " =? WHERE id = ?";

        foreach ($prepareData as $value) {
            $pieceSql = substr_replace(
                $pieceSql,
                " = '" . $value . "'", 
                strpos($pieceSql, ' =?'), 3
                );
        }

        $sql = "UPDATE {$this->tableName} SET " . $pieceSql;
        
        return $sql;
    }

    public function prepareData()
	{   
	    $prepareData = array_intersect_key($this->data, $this->fieldNames);
	    $prepareData = array_map("addslashes", $prepareData);

	    foreach ($prepareData as $key => $value) {
	        if ($value == '') {
	            unset($prepareData[$key]);
	        }

	        if ($key == 'first_name' ||
	        	$key == 'last_name') {
	        	$prepareData[$key] = ucfirst($prepareData[$key]);
	        }
	    }
	    return $prepareData;
	}

}
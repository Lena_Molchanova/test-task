<?php

class Validator
{
	static $errors = [];
	static $model;
    
    /**
     * check - starts validation of values by rules
     * 1. Start rule method:
     *    -> if rule is assoc array -> start rule method $key($checkingVal, array $values);
     *    -> else -> start rule method value($checkingVal)
     *
     * @param array $data - input data;
     * @param string $ruleList - name of list of rules;
     *
     * @return array $errors;
     */
    public static function check(array $data, $ruleList = 'basic')
    {
        static::$model = new Student;

        $validateRules = $ruleList . 'ValidateRules';
        $rules = static::$model::$validateRules($data);

        foreach ($rules as $field => $ruleList) {                

            foreach ($rules[$field] as $keyRule => $rule) {

            	$data[$field] = $data[$field] ?? '';

                is_array($rule) ? 
                static::$keyRule($rule, $data[$field], $field) : 
                static::$rule($data[$field], $field); 
            }                       
        }

        return static::$errors;
    }

    public static function isntEmpty($checkingVal, $checkingField) 
    {
        if (empty($checkingVal)) {
            static::$errors[$checkingField] = $checkingField . " can't be empty";

        }
    }

    public static function isntExistInTable($checkingVal, $checkingField)
    {   
        $result = static::$model->getFieldBy($checkingField, $checkingVal);

        if ($result) {
            static::$errors[$checkingField] = "Such " . $checkingField . " already exists";
        }
    }

    public static function isitEmail($checkingVal, $checkingField)
    {
        if (!filter_var($checkingVal, FILTER_VALIDATE_EMAIL)) {

            static::$errors[] = "This $checkingVal email address is considered valid.";

        }
    }

    public static function isLenghtCorrect(array $args, $checkingVal, $checkingField)
    {
        if (strlen($checkingVal) < $args[0] || 
            strlen($checkingVal) > $args[1] ) {

            static::$errors[] = $checkingField . " must contain from " . $args[0] . " to " . $args[1] . " characters";
       }
    }

    public static function isSumCorrect(array $args, $checkingVal, $checkingField)
    {
        if ($checkingVal < $args[0] || 
            $checkingVal > $args[1] ) {

            static::$errors[] = $checkingField . " must be from " . $args[0] . " to " . $args[1];
       }
    }

    public static function isitNumber($checkingVal, $checkingField)
    {
        if ((int)$checkingVal != $checkingVal) {

            static::$errors[] = $checkingField . " must be an integer";

        }
        
    }

    public static function isntExistInTableExceptMe($checkingVal, $checkingField)
    {
        $result = static::$model->getFieldExceptMeBy($checkingField, $checkingVal);

        if ($result) {
            static::$errors[] = "Such " . $checkingField . " already exists";
        }
    }

}

<?php

class StudentController extends Controller
{
	public function indexAction() 
	{
		$studentModel = new Student;
		$students = $studentModel->getAllStudents();

		$pagination = 3;

		$countPage = ceil(count($students)/$pagination);

		if (isset($_GET['page'])) {

			$start = ($_GET['page'] - 1) * $pagination;
			$students = array_slice($students, $start, $pagination);

		} else {

			$students = array_slice($students, 0, $pagination);
		}

		//SetCookie("registerId", '');

		$this->view->render('layout.php', 'students.php', compact('students', 'countPage'));
	}

	public function registerAction() 
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$studentData = $_POST['student'];

			$errors = Validator::check($studentData);

			if (count($errors) > 0) {

                $this->view->render('layout.php', 'form.php', compact('errors', 'studentData'));

            } else {

                $model = new Student;
                $student_id = $model->save($studentData);
				
				SetCookie("registerId", $student_id ,time()+3600*24*365*10);

                $this->view->render('notification.php', null);

            }
			
		} else {

			$this->view->render('layout.php', 'form.php');
		}
	}

	public function editAction()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			$studentData = $_POST['student'];

			$errors = Validator::check($studentData, 'edit');

			if (count($errors) > 0) {

                $this->view->render('layout.php', 'form.php', compact('errors', 'studentData'));

            } else {

                $model = new Student;
                $model->update($_COOKIE['registerId'], $studentData);

                $this->view->render('notification.php', null);

            }
			
		} else {

			$model = new Student;

			$studentData = $model->getStudent($_COOKIE['registerId']);

			$this->view->render('layout.php', 'form.php', compact('studentData'));
		}
	}
}

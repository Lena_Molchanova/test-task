<?php

define('ROOT', __DIR__);

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'config.php';
require_once 'vendor/bootstrap.php';
